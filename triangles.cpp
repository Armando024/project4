/*
 * CSc103 Project 4: Triangles
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include "triangles.h" // import the prototypes for our triangle class.
#include <iostream>
using std::cout;
#include <vector>
using std::vector;
#include <algorithm>
using std::sort;

// note the "triangle::" part.  We need to specify the function's
// FULL name to avoid confusion.  Else, the compiler will think we
// are just defining a new function called "perimeter"
unsigned long triangle::perimeter() {

	return s1+s2+s3;
}

unsigned long triangle::area() {
	// TODO: write this function.
	// Note: why is it okay to return an integer here?  Recall that
	// all of our triangles have integer sides, and are right triangles...
	//unsigned long q = ;
	unsigned long Z[3]={s1,s2,s3};
	sort(Z,Z+3);

	return (Z[0]*Z[1])/2;
}

void triangle::print() {
	cout << "[" << s1 << "," << s2 << "," << s3 << "]";
}

bool congruent(triangle t1, triangle t2) {
	// TODO: write this function.

	unsigned long A[3]={t1.s1,t1.s2,t1.s3};
	unsigned long B[3]={t2.s1,t2.s2,t2.s3};
	sort(A,A+3);
	sort(B,B+3);
	int w=0;

	if (A[0]==B[0]) {w++;}
	if (A[1]==B[1]) {w++;}
	if (A[2]==B[2]) {w++;}

	if (w==3) {return true;}
	else { return false;}
}

bool similar(triangle t1, triangle t2) {

	unsigned long triangle1[3]={t1.s1,t1.s2,t1.s3};
	unsigned long triangle2[3] ={t2.s1,t2.s2,t2.s3};
		sort(triangle1,triangle1+3);
		sort(triangle2,triangle2+3);

if (triangle1[0] > triangle2[0]) {
	if(triangle1[0] % triangle2[0] == 0) {
			if 	(triangle1[1] % triangle2[1] == 0) {
					if (triangle1[2] % triangle2[2] == 0) {return true;}
		} }
	}

if (triangle1[0]<triangle2[0]) {

	if(triangle2[0] % triangle1[0] == 0) {
			if 	(triangle2[1] % triangle1[1] == 0) {
					if (triangle2[2] % triangle1[2] == 0) {return true;}
		} }
	}

	return false;

}

vector<triangle> findRightTriangles(unsigned long l, unsigned long h) {
	// TODO: find all the right triangles with integer sides,
	// subject to the perimeter bigger than l and less than h

	vector<triangle> retval; // storage for return value.

	triangle R;
	for(unsigned long i = 1;i < 75;  i++){
		R.s1 = i;
	  for(unsigned long k = 1; k < 75; k++){
			R.s2 = k;
	    for(unsigned long r = 1; r < 75; r++){
				R.s3 = r;

				if(R.perimeter()  >= l && R.perimeter() <= h){
					unsigned long A[3]={R.s1,R.s2,R.s3};
					sort(A,A+3);
				  if(((A[0] * A[0]) + (A[1] * A[1])) == (A[2] * A[2]) )	{
					  retval.push_back(R);
				  }

				}
	}
		}
			}
	return retval;

}
